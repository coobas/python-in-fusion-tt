# Programování a analýza dat (nejen) v Pythonu v termojaderné fúzi


---
## Thermonuclear fusion

+++?image=figs/fusion_process.png
### What is fusion

- Fusion is the energy source of the Sun and stars.
- We are trying to make use of the fusion of two hydrogen isotopes, deuterium (D) and tritium (T).
- The fusion energy released from just 1 gram of DT equals the energy from about 2400 gallons of oil.
- E = mc^2.
- Tokamak confines hot plasma in a torus by a strong magnetic field.


+++

![Fusion Energy Explained](https://www.youtube.com/embed/N4yWhA1mVxA)


+++?image=https://www.iter.org/doc/www/content/com/Lists/WebText_2014/Attachments/230/tkm_cplx_final_plasma2013-07.jpg
### ITER = International Thermonuclear Experimental Reactor and also Latin for "the way"


---
## Integrated modelling


+++
### Motivation and challenges

- Modelování má pro fúzi dva základní významy:
  1. Porozumění současným experimentům.
  2. Predikce pro budoucí zařízení.
- Obrovský rozsah škál jevů, které spolu interagují.

![Scales](figs/plasma_scales.png)


+++
### Computational challenges

- Integrace mnoha komponent -> workflow (pipelines) přístup.
- Problém rozhraní mezi kódy:
  - N kódů --> až N2 rozhraní (reálně daleko méně).
  - M programovacích jazyků --> ještě více rozhraní.
- Je potřeba jednotný datový model a vícejazyčné rozhraní.
- Legacy codes.
- Scientists are mostly not software engineers!


---
## Particular (Pythonic) projects


+++
### CDB = COMPASS DataBase

- Goal: Store all experimental and analysis data from the COMPASS tokamak.
  - Multidimensional data (time, space, ...), videos, *.
  - Keep track of data sources (ADC's, postprocessing, ...)
  - ~ GB's per discharge, ~10's discharges / experimental day.
  - Clients for C/C++, Java, Python, Matlab, IDL, ???.

https://bitbucket.org/compass-tokamak/cdb/src
https://webcdb.tok.ipp.cas.cz/


+++
### CDB technology

- Data in files (mostly HDF) across filesystems.
  - Easily scalable, standard technology.
- Metadata cataloque in (My)SQL database.
  - Fast lookup due to indexes, foreign keys, ...
- Core functionality in Python.
  - Slim bindings to other languages, mostly via C-API exported via Cython.
  - Some REST API.
No data overwrite possible -- use revisions.

```python
from pyCDB.client import CDBClient
cdb = CDBClient()

signal = cdb.get_signal('I_plasma:10999')

cdb.put_signal(
  signal.generic_signal_id,
  109999,
  processed(signal.data)
)

```


+++
### WOW:-P = a WOrkfloW framework in Python

- Enables [flow based programming](https://en.wikipedia.org/wiki/Flow-based_programming) in Python via [actors](https://en.wikipedia.org/wiki/Actor_model) and workflows.
  - Similar to functional / reactive programming.
  - Actors have ports and states and react to incoming data.
  - Motivated by (replacing) [Kepler](https://kepler-project.org).
- Clean Python API, which also creates a declarative (domain specific) language.
- Heavily leverage `concurent.futures` for concurrency.
  - Can use the `Executor` API of [`mpi4py`](https://github.com/mpi4py/mpi4py), [`ipyparallel`](https://github.com/ipython/ipyparallel) or [`dask.distributed`](https://github.com/dask/distributed)
- Internally processes a directed graph (cycles possible although dangerous.)

https://github.com/coobas/wowp


+++
### Easy actors and workflows

```python
def func1(x) -> ('a'):
    return x * 2

def func2(x, y) -> ('a'):
    return x + y

in_actor1 = FuncActor(func1)
in_actor2 = FuncActor(func1)
out_actor = FuncActor(func2)

# connect actors
out_actor.inports['x'] += in_actor1.outports['a']
out_actor.inports['y'] += in_actor2.outports['a']
```


+++
### Actor API

```python
class ToStrActor(Actor):

    def __init__(self, *args, **kwargs):
        super(StrActor, self).__init__(*args, **kwargs)
        # specify input port(s)
        self.inports.append('input')
        # and output port(s)
        self.outports.append('output')

    def get_run_args(self):
        # get input value(s) using .pop()
        args = (self.inports['input'].pop(), )
        kwargs = {}
        return args, kwargs

    @staticmethod
    def run(value):
        # return a dictionary with port names as keys
        return {'output': str(value)}
```
Although this is simply
```python
ToStrActor = ConstructorWrapper(FuncActor, str)
```


+++
### Advanced WOW:-P

- Meta-actors `Map` and `Chain`, can execute concurrently.

```python
Sin = ConstructorWrapper(FuncActor, np.sin)
Annotate = ConstructorWrapper(PassWID)
chain = Chain('chain', (Sin, Annotate))
```
Can be simply `__call__`ed:
```python
chain(inp=np.pi/2)['out']
```
or run in parallel:
```python
map_parallel = Map(
Chain,
args=('chain', (Sin, Annotate), ),
scheduler=FuturesScheduler('distributed'))
map_parallel(inp=np.linspace(0, np.pi))
```


+++
### HDC = Hierarchical Dynamic Containers

- Goal: Communicate complex tree structured data between simulation codes in different languages.
- Design: Mimic Python's dict, list, numpy with data buffers in a key-value store.
  - Use shared memory via [mdbm](https://github.com/yahoo/mdbm).
  - Enable zero copy (even between different languages) -- via shared mem.
- Implementation: C++ code (a lot of boost), bindings for Python (via Cython) and C (--> Fortran and others).

https://bitbucket.org/compass-tokamak/hdc


+++
### HDC data types

- Empty (null type)
- Ordered map - a tree internal node
- List (sequence) - a non-homogeneous dynamic array
- Multi-dimensional homogeneous arrays of primitive types
- String – any size, any character (Unicode)


+++
### HDC example
```python
tree = HDC()

tree['group/data'] = np.linspace(0, 1)
tree['meta/desc'] = 'I ❤ HDC'

ctypes.cdll.LoadLibrary('fortran_module.so').hdc_to_fortran(tree)
assert np.asarray(tree[group/data])[-1] == 2
```

```fortran
subroutine hdc_to_fortran(tree) bind(c, name="hello_f")
    use hdc_fortran
    use iso_c_binding
    type(hdc_t), value :: tree
    real(kind=DP), dimension(:), pointer :: x

    call hdc_get(tree, 'group/data', x)
    x = 2 * x
```


+++
### Residues

- [pydons](https://github.com/coobas/pydons)
  - `MatStruct` ordered dict with string-only keys accessible as properties.
  - `LazyDataset` proxy for data sets in HDF5 or netCDF4 files.
  - `FileBrowser` employs MatStruct and LazyDataset for fast browsing of netCDF4 or HDF5 files.
- [mqcontrol](https://bitbucket.org/urbanj/mqcontrol)
  -  A ligh-weight, asynchronous interface between Simulink and other languages (e.g. Python) via a message queue / message broker system, in particular Redis.


---
## Python lecturing @ FNSPE

This is fun since 2015 :)

https://jakuburban.info/python_fjfi/



<!-- +++
### History

- Jak jsem začal s Pythonem
- JvdP IDL vs Python
- Fortran a spol. -->

---
## A bit of science

+++
## Disruption detection

+++
### Disruptions must be avoided

- Distruption is a kind of explosion.
- Must be avoided in power plant scale tokamaks.
- Physics behind unclear.
- Can we use machine learning for prediction?
- https://logbook.tok.ipp.cas.cz/index.php?show=shot&shot_no=14665
- https://webcdb.tok.ipp.cas.cz/data_signals/728/14665


+++
### ML prediction results

- Features = values or statistics of filtered time signals.
  - Causal filters.
  - Entropy, comlexity estimation, Fourier.
- Random forests perform quite well.
- Best results with autoencoders + a neural net classifier.
  - accuracy 82 %, recall 86 %, false positive / negative 22 % / 14 %


+++
### Bayesian scenario optimization

- Goal: Optimize tokamak operation scenario (e.g. for performace).
  - Use a fast integrated modelling tool.
  - And a suitable optimizer.
- Bayesian optimization
  - finds global maximum expensive objective functions
  - using Gaussian process as a surrogate function
  - and a utility function which trades off exploration and exploitation.
- https://github.com/fmfn/BayesianOptimization

<!-- - Hidden Markov chain H/L mode transition
- Detekce anomálií -->
